package com.example.rafalsuski.retrofittest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Rafal Suski on 25.02.2018.
 */

public interface GetDataService {

    @GET("/photos")
    Call<List<RetroPhoto>> getAllPhotos();
}
//